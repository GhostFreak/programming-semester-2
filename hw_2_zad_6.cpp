/***
FN:F79506
PID:6
GID:1
*/

#include <iostream>
#include <fstream>
#include <cctype>
#include <string>
#include <algorithm>

using namespace std;

int main(int argc, char * argv[])
{
	//code here
	ifstream inp_data;
	for(int i=2;i<argc;i++)
	{
		
		string ch;
	
		inp_data.open(argv[i]);
	
		int count=0;
		string file;
		while(inp_data>>ch)
		{
			for(int j=0;j<ch.length();j++)
			{
			
			if(ispunct(ch[j])) ch.erase(j,ch.length());
			
			if(isupper(ch[j])) ch[j]=tolower(ch[j]);
		} 
			if(ch==argv[1])
			{
				count++;
				file=argv[i];
			}
		}
		if(count!=0)
		cout<<file<<" "<<count<<endl;
		inp_data.close();
	}
	
	return 0;
}
