#include <iostream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;

class Worker
{
	public:
	virtual void setPayment(double c){}
	
};

class HourlyWorker : public Worker
{
	protected:
	string name;
	double salary;
	
	public:
	HourlyWorker(string name,double salary)
	{
		this->name=name;
		this->salary=salary;
	}
	
	virtual void setPayment(double c) 
	{
		double h=c;
		double pay=0.00;
		if(c>40)
		{
			c=40;
			pay= salary*c;
		}
		else
		pay= salary*c;
		
		cout<<name<<" got "<<pay<<" for "<<h<<" hours"<<endl;
	}


};
class SalariedWorker : public Worker
{
	protected:
		string name_;
		double salary_;
	public:
		SalariedWorker(string name_,double salary_)
		{
			this->name_=name_;
			this->salary_=salary_;
		}
		
	virtual void setPayment(double c)
	{
		double pay=0.00;
		if(c>40)
		{
			double d = ((c-40)*salary_)*1.5;
			pay= (40*salary_)+d;
		}
		else
		{
			pay= c*salary_;
		}
		cout<<name_<<" got "<<pay<<" for "<<c<<" hours"<<endl;
	}

};

int main()
{
/*	Worker*h= new HourlyWorker("Henry", 10);
	Worker*a= new HourlyWorker("Andrea", 10);
	Worker*b= new SalariedWorker("Borko", 10);
	Worker*v= new SalariedWorker("Victor", 10);	
	h->setPayment(40.00);
	a->setPayment(45.00);
	b->setPayment(40.00);	
	v->setPayment(45.00);
	*/
	vector<Worker*>V;
	string name;
	string type;
	double hours, salary;
	int N,n,num;
	cin>>N;
	for(int i=0;i<N;i++)
	{
		cin>>type;
		if(type=="s")
		{
			cin>>name>>salary;
			V.push_back(new HourlyWorker(name,salary));
		}
			if(type=="h")
		{
			cin>>name>>salary;
			V.push_back(new SalariedWorker(name,salary));
		}
	}
	cin>>n;
	for(int i=0;i<n;i++)
	{
		cin>>num>>hours;
		V[num-1]->setPayment(hours);
		
	}
	
	
	return 0;
}

