/***
FN:F79506
PID:1
GID:1
*/

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

 double sum_num(ifstream &in)
{
	double sum=0.00;
	double next_num;
	
	while(in>>next_num)
	{
		sum=next_num+sum;
	}
	
	return sum;
}

 double avg_num(ifstream &in)
{
	double avrg;
	double sum=0.00;
	double count=0.00;
	double next_num;
	while(in>>next_num)
	{
		sum=next_num+sum;
		count++;
	}
	avrg=sum/count;
	
	return avrg;
}

 double max_num(ifstream &in)
{
	double max;
	double next_num;
	in>>next_num;
	max=next_num;
	while(in>>next_num)
	{
		if(next_num>max) max=next_num;
	}
	
	return max;
}

double min_num(ifstream &in)
{
	double min;
	double next_num;
	in>>next_num;
	min=next_num;
	while(in>>next_num)
	{
		if(next_num<min) min=next_num;
	}
	
	return min;
}

int main(int argc, char * argv[])
{
	//code here
	ifstream inp_data;
	
	
	inp_data.open(argv[1]);
	if (!inp_data.is_open())
 {
  return -1;
 }
	double sum=sum_num(inp_data);
	inp_data.close();
	
	inp_data.open(argv[1]);
	double avg=avg_num(inp_data);
	inp_data.close();
	
	inp_data.open(argv[1]);
	double max=max_num(inp_data);
	inp_data.close();
	
	inp_data.open(argv[1]);
	double min=min_num(inp_data);
	inp_data.close();
	
	cout<<"SUM: "<<sum<<endl;
	cout<<"AVG: "<<avg<<endl;
	cout<<"MIN: "<<min<<endl;
	cout<<"MAX: "<<max<<endl;

	return 0;
}
