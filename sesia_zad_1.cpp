/***
FN:F00000
PID:1
GID:1
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>
#include <algorithm>

using namespace std;

class Person
{
	public:
		Person(string name= "",string fname="")
		{
			this->name=name;
			this->fname=fname;
		}
		string getname() const
		{
			return name;
		}
		string getfname() const
		{
			return fname;
		}
virtual void read(istream &input)
{
	input>>name;
	input>>fname;
}

virtual void write(ostream &output)
{
	output<<setw(14)<<left<<name<<" "<<setw(14)<<left<<fname;
}

bool operator<(const Person&right)
{
	if(name!=right.name) 
	return name<right.name;
	else 
	return fname<right.fname; 
}
	protected:
		string name;
		string fname;
			
};
class Student:public Person
{
	public:
		Student(string name="",string fname="",string fnumber="")
		{
			this->name=name;
			this->fname=fname;
			this->fnumber=fnumber;
		}
		string getfnumber() const
		{
			return fnumber;
		}
		virtual void read(istream &is)
		{
			Person:: read(is);
			is>>fnumber;
		}
		virtual void write(ostream &os)
		{
			Person:: write(os);
			os<<" "<<fnumber;
		}
	private:
		string fnumber;
			
};

ostream & operator << (ostream &os, Person *p)
{
	p->write(os);
	return os;
}
istream & operator >> (istream &is, Person *p)
{
	p->read(is);
	return is;
}

//compare functioin, DO NOT CHANGE OR REMOVE
bool comp(Person *a, Person *b)
{
	return *a < *b;
}

int main(int argc, char * argv[])
{
	vector<Person *> people;

	//code your input here
	ifstream input;
	input.open(argv[1]);
	Person *p = new Person();
	while(input>>p)
	{
		people.push_back(p);
		p = new Person();

	}
	delete p;
	input.close();
	input.open(argv[2]);
	Person *s = new Student();
	while(input>>s)
	{
		people.push_back(s);
		s = new Student();

	}
	delete s;
	input.close();

	/*read the people from the files using the stream operator >>, for example, if 'in' is an input stream:
	Person *p = new Person();
	while (in >> p)
	{
		people.push_back(p);
		p = new Person();
	}
	delete p;*/
	

	sort(people.begin(), people.end(), comp); //DO NOT CHANGE OR REMOVE THIS CODE

	//code your output here
	ofstream output;
	output.open(argv[3]);
	for(int i=0;i<people.size();i++)
	{
		output<<people[i]<<endl;;
	}
	output.close();


	return 0;
}
