/***
FN:F00000
PID:1
GID:1
*/

#include <iostream>
#include <string>
#include <stdexcept>
#include <fstream>
using namespace std;

template<typename T>
class List
{
public:
	List()
	{
		capacity=0;
	    count=0;
	    elements=new T[capacity];
	}
	List(const List<T> &other)
	{
		capacity=other.capacity;
	    count=other.count;
		elements=new T[other.capacity];
		memcpy(elements, other.elements, sizeof(T) * capacity);
	}
	List<T>& operator=(const List<T> &right)
	{
		delete []elements;
		capacity=right.capacity;
		count=right.count;
		elements =new T[capacity];
		
		for(int i=0;i<count;i++)
		{
			elements[i]=right.elements[i];
		}
	}

	~List()
	{
		delete [] elements;
	}

	T get(int i) const
	{
		return elements[i];
	}
	void add(T data)
	{
		resize(count+1);
        elements[count]=data;
        count++;
	}
	T pop()
	{
		count--;
		return elements[count];
	}
	int getCount() const
	{
		return count;
	}

	T& operator[](unsigned int index)
	{
		if(index < 0 or index > count-1) 
		throw out_of_range("OUT OF RANGE");
		
		else
		return elements [index];
	}
	T& operator[](unsigned int index) const
	{
		if(index < 0 or index > count-1) 
		throw out_of_range("OUT OF RANGE");
		
		else
		return elements [index];
	}

	List<T> operator+(List<T> &right)
	{
		int c=(capacity+right.capacity)+2;
		T*elements3=new T[c];
		for(int i=0;i<count;i++)
		{
			elements3[i]=elements[i];
		}
		int i=count;
		for(int j=0;j<right.count;j++)
		{
			elements3[i]=right.elements[j];
			i++;
		}
	
		//delete [] elements3;
		
		
	}

private:
	T *elements;
	int capacity;
	int count;

	//use this function to allocate a new, bigger array of elements to fit the added values. Don't forget to delete the old array
	void resize(int newCapacity)
	{
		T*elements2=new T [newCapacity];
	    for(int i=0;i<count;i++)
        {
            elements2[i]=elements[i];
        }
        delete [] elements;
        elements=elements2;
        capacity=newCapacity;
	}
};
template <typename T>
ostream& operator<<(ostream& os, List<T>&list)
{
	for(int i=0;i<list.getCount();i++)
	{
		os<<list[i]<<" ";
	}
	return os;
}
template <typename T>
istream& operator>>(istream& in, List<T>&list)
{
	T income;
	while(in>>income)
	{
		list.add(income);
	}
	return in;

}

template<typename T>
void print(List<T> list)
{
	for (int i = 0; i < list.getCount(); i++)
		cout << list.get(i) << " ";
	cout << endl;
}

template<typename T>
void print2(const List<T> &list)
{
	for (int i = 0; i < list.getCount(); i++)
		cout << list[i] << " ";
	cout << endl;
}

int main(int argc, char* argv[])
{
	List<float> list1;
	list1.add(1.0f);
	list1.add(2.0f);
	list1.add(3.0f);
	cout << list1.get(0) << " " << list1.get(1) << " " << list1.get(2) << endl;

	List<float> list2;
	list2 = list1;
	cout << list2.get(0) << " " << list2.get(1) << " " << list2.get(2) << endl;

	list2.add(4.0f);
	cout << list2.get(0) << " " << list2.get(1) << " " << list2.get(2) << " " << list2.get(3) << endl;
	cout << list1.pop() << endl;
	cout << list1.getCount() << endl;

	print(list1);
	print(list1);
	print(list2);

	cout << "--" << endl;

	print2(list1);
	cout << list1 << endl;
	cout << list2 << endl;

	cout << list1[1] << endl;
	list2[2] += 10;
	cout << list2[2] << endl;

	List<string> list4;
	cin >> list4;
	cout << list4 << endl;

	cout << "--" << endl;

	List<float> list3 = list1 + list2;
	cout << list1 << endl;
	cout << list2 << endl;
	cout << list3 << endl;

	try
	{
		cout << list2[4] << "!" << endl;
	}
	catch (out_of_range &e)
	{
		cout << "Out of range exception was thrown." << endl;
	}

	return 0;
}
