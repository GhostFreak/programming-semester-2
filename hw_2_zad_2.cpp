/***
FN:F79506
PID:2
GID:1
*/

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <iomanip>

using namespace std;

int main(int argc, char * argv[])
{
	//code here
	ifstream inp_data;
	ofstream out_data;
	inp_data.open(argv[1]);
	out_data.open(argv[2]);
	if (!inp_data.is_open()&&!out_data.is_open())
	{
		return -1;
	}
	int next;
	int number;

	while(inp_data>>next)
	{
	
		 if (argv[3][0] == '+') 
        {
            number = next + atoi(argv[4]); 
            out_data << number << " ";
        }
         if (argv[3][0]== '-')
        {
            number = next - atoi(argv[4]);
            out_data << number << " ";
        }
         if (argv[3][0] == 'x')
        {
            number = next*atoi(argv[4]);
            out_data << number << " ";
        }
		
	}

	inp_data.close();
	out_data.close();
	
	return 0;
}
