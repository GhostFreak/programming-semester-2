/***
FN:F00000
PID:2
GID:1
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;

void print(vector<double>&v)
{
	for(int i=0;i<v.size();i++)
	{
		cout<<v[i]<<endl;
	}
}

void sort(vector<double>&v)
{
	double min;
	int j=0;
	for(int i=0;i<v.size()-1;i++)
	{
		min=v[i];
		j=0;
		
		for(j=(i+1);j<v.size();j++)
		{
			if(min>v[j])
			{
			
				double num=min;
				min=v[j];
				v[j]=num;
			}
		}
		
		v[i]=min;
		
	}
}

int main(int argc, char * argv[])
{
	vector<double>v;
	ifstream data;
	for(int i=1;i<argc;i++)
	{
		double num;
		data.open(argv[i]);
		while(data>>num)
		{
			v.push_back(num);
		}
		data.close();
	}

sort(v);
print(v);
	
	return 0;
}
