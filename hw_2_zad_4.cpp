/***
FN:F79506
PID:4
GID:1
*/

#include <iostream>
#include <cstdlib>

using namespace std;
long long rec[32];

long long recurs(int n)
{
	if(rec[n]>0)
	{
		return rec[n];
	}
	if(n<=3)
	{
		rec[n]=1;
		return rec[n];
	} 
	rec[n]=(3*recurs(n-1))-(2*recurs(n-2))+recurs(n-3);
		return rec[n];
	
	
}

long long itr(int n)
{
	long long result;
	if(n==1) return 1;
	if(n==2) return 1;
	if(n==3) return 1;
	else
	{
		long long n1=1;
		long long n2=1;
		long long n3=1;
		for(int i=3;i<n;i++)
		{
			result=3*n3-2*n2+n1;
			n1=n2;
			n2=n3;
			n3=result;
		}
		return n3;
	}

}

int main(int argc, char * argv[])
{
	//code here
	int n=atoi(argv[1]);
	long long result_1=recurs(n);
	long long result_2=itr(n);
	
	cout<<result_1<<endl;
	cout<<result_2<<endl;
	
	return 0;
}
