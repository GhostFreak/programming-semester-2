/***
FN:F79506
PID:5
GID:1
*/

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
using namespace std;
void printVector(vector<int>& A)
{
	for(int i=0;i<A.size();i++)
	{
		 cout<<A[i]<<" ";
	}
}


void bubbleSort(vector<int>& A)
{
	bool flag=true;
	while(flag)
	{
		flag=false;
		for(int i=0;i<A.size()-1;i++)
		{
			if(A[i] > A[i+1]) 
			{
			swap(A[i],A[i+1]);
			flag=true;
			}
		}
		printVector(A);
		cout<<endl;

		
	}
	
}

void selectionSort(vector<int>& A)
{
	int min;
	int j=0;
	for(int i=0;i<A.size()-1;i++)
	{
		min=A[i];
		j=0;
		
		for(j=(i+1);j<A.size();j++)
		{
			if(min>A[j])
			{
			
				int num=min;
				min=A[j];
				A[j]=num;
			}
		}
		
		A[i]=min;
		printVector(A);
    	cout<<endl;
	}

}
	
		
int main()
{
	int n;
	cin >> n;
	vector<int> A(n);
	for (int i = 0; i < n; i++) cin >> A[i];

	vector<int> bsCopy(A.begin(), A.end());
	bubbleSort(bsCopy);

	cout << endl;

	vector<int> ssCopy(A.begin(), A.end());
	selectionSort(ssCopy);

	return 0;
}

/*void bubbleSort(vector<int>& A)
{
	/*
	n = length(A)
	do
		swapped = false
		for i = 1 to n-1 inclusive do
			//if this pair is out of order
			if A[i-1] > A[i] then
				//swap them and remember something changed
				swap( A[i-1], A[i] )
				swapped = true
			end if
		end for

		print the vector

	while not swapped
	
	
}

void selectionSort(vector<int>& A)
{
	/*
	for j = 1 to n - 1 exclusive do
		iMin = j
		for i = j + 1 to n exclusive do
			if A[i] < A[iMin] then
				iMin = i
			end if
		end for

		if iMin != j then
			swap( A[j], A[iMin] )
		end if

		print the vector
	end for
	
}

void printVector(vector<int>& A)
{
	for (int i = 0; i < A.size(); i++)
		cout << A[i] << (i == A.size() - 1 ? "\n" : " ");
}*/

