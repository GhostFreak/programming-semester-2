/***
FN:F79506
PID:1
GID:1
*/
#include <iostream>
#include <stdexcept>

using namespace std;

//implement the functions directly in the class declaration below
template <typename T>
class Array
{
public:
	Array(int size)
	{
		this->size=size;
		elements=new T[size];
		for(int i=0;i<size;i++)
		{
			elements[i]=0;
		}
	}
	Array(const Array& right) 
	{
		size=right.size;
		elements=new T[right.size];
		memcpy(elements, right.elements, sizeof(T) * size);
		
	}
	Array& operator=(const Array& right)
	{
		if(size!=right.size)
		{
			delete []elements;
			size=right.size;
			elements =new T[size];
		}
		for(int i=0;i<size;i++)
		{
			elements[i]=right.elements[i];
		}
		
	}


	virtual ~Array()
	{
		delete []elements;
	}

	T& operator[](int index)
	{
		if(index < 0 or index > size) 
		throw out_of_range("OUT OF RANGE");
		
		else
		return elements [index];
	}

	T& operator[](int index) const
	{
		return elements [index];
	}

	int get_size() const
	{
		return size;
	}

	void sort_array()
	{
	int min;
	int j=0;
	for(int i=0;i<size-1;i++)
	{
		min=elements[i];
		j=0;
		
		for(j=(i+1);j<size;j++)
		{
			if(min>elements[j])
			{
			
				int num=min;
				min=elements[j];
				elements[j]=num;
			}
		}
		
		elements[i]=min;
	}
	
	}
private:
	T* elements;
	int size;
};

template <typename T>
ostream& operator<<(ostream& os, Array<T> arr)
{
	for(int i=0;i<arr.get_size();i++)
	{
		os<<arr[i];
	}
	return os;
}
int main()
{
	//do not change this
	Array<int> arr(5);
	arr[0] = 1;
	arr[1] = 2;

	cout << arr << endl;
	cout << "arr.size= " << arr.get_size() << endl;

	try
	{
		cout << arr[6] << endl;
	}
	catch (const out_of_range& e)
	{
		cout << "6 is out of range" << endl;
	}

	arr.sort_array();
	cout << arr << endl;

	return 0;
}
