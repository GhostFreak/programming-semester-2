/***
FN:F00000
PID:2
GID:1
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>
#include <algorithm>

using namespace std;

template<typename T>
class Collection
{
public:
	Collection(int size=0)
	{
		this->size=size;
		elements = new T [size];
		for(int i =0;i<size;i++)
		{
			elements[i]=0;
		}
	}
	Collection(const Collection<T> &other)
	{
		size=other.size;
		elements=new T[other.size];
		memcpy(elements, other.elements, sizeof(T) * size);
	}
	Collection<T>& operator=(const Collection<T> &right)
	{
		if(this!=&right)
		{
			delete [] elements;
			size=right.size;
			elements = new T [size];
			for(int i =0;i<size;i++)
			{
			elements[i]=right.elements[i];
			}
			
		}
		return *this;
	}

	~Collection()
	{
		delete [] elements;
	}

	const int get_size() const
	{
		return size;
	}

	T& operator[](unsigned int index)
	{
		return elements[index];
	}
	T& operator[](unsigned int index) const
	{
		return elements[index];
	}
private:
	int size;
	T* elements;
};

template<typename T>
ostream& operator<<(ostream& os, const Collection<T>& c)
{
	for(int i=0;i<c.get_size();i++)
	{
		os<<c[i]<<" ";
	}
	return os;
}

template<typename T>
istream& operator>>(istream& is, const Collection<T>& c)
{
	for(int i=0;i<c.get_size();i++)
	{
		is>>c[i];
	}
	return is;
}

int main(int argc, char * argv[])
{
	Collection<int> integers(4);
	cin >> integers;
	cout << "Integers:" << endl << integers << endl;

	Collection<double> doubles(6);
	cin >> doubles;
	cout << "Doubles:" << endl << doubles << endl;

	if (doubles.get_size() > 0)
	{
		Collection<double> last;
		last = doubles;

		for (int i = 0; i < last.get_size(); i++)
		{
			last[i]++;
		}

		cout << "Modified doubles:" << endl << last << endl;
	}
	cout << "Original doubles:" << endl << doubles << endl;

	return 0;
}
