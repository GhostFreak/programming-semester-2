/***
FN:F79506
PID:2
GID:1
*/

#include <iostream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;
class Shape
{
	public:
	virtual void print_P() const =0;
	virtual void print_S() const =0;	
};
class Triangle: public Shape
{
	protected:
	double a, b, c;
	public:
	Triangle(double a_,double b_,double c_): a(a_), b(b_), c(c_) {}
	virtual void print_P() const
	{
		cout<<a+b+c<<endl;
	}
	virtual void print_S() const
	{
		double p=(a+b+c)/2;
		double s=p*(p-a)*(p-b)*(p-c);
		cout<<sqrt(s)<<endl;
	}
};
class Rectangle: public Shape
{
	protected:
	double d,e;
	public:
	Rectangle( double d_, double e_):d(d_), e(e_){}
	virtual void print_P() const
	{
		cout<<(d+e)*2<<endl;
	}
	virtual void print_S() const	
	{
		cout<<d*e<<endl;
	}
	
};


int main()
{
	vector<Shape*>V;
    double a,b,c;
	int n,N;
	char ch;
	cin>>n;
	for(int i=0;i<n;i++)
	{
		cin>>ch;
		if(ch=='t')
		{
			cin>>a>>b>>c;
			V.push_back(new Triangle(a,b,c));
		}
		if(ch=='r')
		{
			cin>>a>>b;
			V.push_back(new Rectangle(a,b));
		}
		
	}
	
	while(cin>>ch)
	{
		cin>>N;
		if(ch=='a') 
		V[N-1]->print_S();
			
		 if(ch=='p') 
		V[N-1]->print_P();
		
	}
	
	
	
	return 0;
}
